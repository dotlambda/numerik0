function integral = aufgabe2(n)
	h = 2^-n;
	f = @(x) exp(-x.^3);
	y = 0:h:1;
	m = length(y) - 1
	integral = h/2 * sum(f(y(1:m)) + f(y(2:m+1)));
end