function erg = aufgabe4()
  x = [-5, -3, -1, -.1, 10^-4, 4, 4.5].';
  y = [1.495, 1.316, 1, .562, .1, 1.414, 1.456].';

  n = length(x);

  % f1
  A(1:n,1) = ones(n,1);
  A(1:n,2) = x .^ 2;
  [a_1, b_1] = loesen(A, y);

  % f2
  A(1:n,1) = ones(n,1);
  A(1:n,2) = exp(- x.^2 / 2);
  [a_2, b_2] = loesen(A, y);

  range = -5:.1:5;
  plot(x, y, 'x', range, f1(a_1,b_1,range), range, f2(a_2,b_2,range));

  fprintf('\n## f1 ##\n');
  fprintf('Max. Abweichung: %f\n', norm(f1(a_1,b_1,x)-y,Inf));
  fprintf('Mittlere Abweichung: %f\n', norm(f1(a_1,b_1,x)-y,2));

  fprintf('\n## f2 ##\n');
  fprintf('Max. Abweichung: %f\n', norm(f2(a_2,b_2,x)-y,Inf));
  fprintf('Mittlere Abweichung: %f\n', norm(f2(a_2,b_2,x)-y,2));
end

function y = f1(a, b, x)
  y = a + b * x.^2;
end

function y = f2(a, b, x)
  y = a + b * exp(- x.^2 / 2);
end

function [a, b] = loesen(A, y)
  A_t = transpose(A);
  [L, R] = lr_zerlegung(A_t * A);
  x = lgs_loesen(L, R, A_t * y);
  a = x(1);
  b = x(2);
end
