function [L] = MyCholBand(A,m)

% Eingabe:  A: Symmetrische und positiv definite n x n Matrix
%           m: Bandweite
% Ausgabe:  L: Linke untere Dreiecksmatrix mit A=L*L'
%
% Beispiel: >>  A=diag(2*ones(5,1)) + diag(-1*ones(3,1),-2)+diag(-1*ones(3,1),2)
%           >>  L=MyCholBand(A,2)
%           ans =
%
%    1.4142         0         0         0         0
%         0    1.4142         0         0         0
%   -0.7071         0    1.2247         0         0
%         0   -0.7071         0    1.2247         0
%         0         0   -0.8165         0    1.1547


[n,nn] = size(A);
L=zeros(n);

L(1,1)   = sqrt(A(1,1));
L(2:2+m,1) = A(2:2+m,1)./L(1,1);

for i=2:(n-m)
        L(i,i) = sqrt(A(i,i) - L(i,1:i-1)*L(i,1:i-1)');
        for j=i+1:i+m
          L(j,i) = (L(i,i)^(-1)) * (A(j,i) - L(i,1:i-1)*L(j,1:i-1)');
        end
end

for i=(n-m)+1:n
        L(i,i) = sqrt(A(i,i) - L(i,1:i-1)*L(i,1:i-1)');
        for j=i+1:n
          L(j,i) = (L(i,i)^(-1)) * (A(j,i) - L(i,1:i-1)*L(j,1:i-1)');
        end
end

end
