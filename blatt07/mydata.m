function erg = mydata(x,y,r,p,n)

% x = x-Koordinate Mittelpunkt
% y = y-Koordinate Mittelpunkt
% r = Radius des Kreises
% p = Faktor zur Skalierung der Gauss-verteilten Stoerung der Punkte
% n = Anzahl der Punkte

% Beispiel:
% >> mydata(0,0,1,0,20);
% Erzeugt 20 aequidistante Punkte auf dem Einheitskreis

alp = linspace(0,2*pi,n)';

noi = 2*r*p*(0.5-rand(length(alp),2));
xp=r*cos(alp)+noi(:,1);
yp=r*sin(alp)+noi(:,2);

erg=[x+xp,y+yp];

end

