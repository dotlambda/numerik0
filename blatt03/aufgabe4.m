function erg = aufgabe4()
  range = 1:5;
  for k = range
    m = 2^k;
    I = -eye(m);
    B = 4*eye(m) + diag(-ones(1, m-1), -1) + diag(-ones(1, m-1), 1);
    for n = 1:m:m^2-2*m+1
      A(n:n+m-1,n:n+m-1) = B;
      A(n+m:n+2*m-1,n:n+m-1) = I;
      A(n:n+m-1,n+m:n+2*m-1) = I;
    end
    A(m^2-m+1:m^2,m^2-m+1:m^2) = B;

    [L,R] = lr_zerlegung(A);
    fehler_lr(k) = norm(A - L * R, Inf);

    L_cholesky = L * diag(diag(R) .^ .5);
    fehler_cholesky(k) = norm(A - L_cholesky * transpose(L_cholesky), Inf);

    for i = 1:m^2
      e = (1:m^2==i)'; % i-ter Einheitsvektor
      Inverse(1:m^2,i) = lgs_loesen(L, R, e);
    end
    I = eye(m^2);
    fehler_inverse(k) = norm(A * Inverse - I, Inf);

    kondition(k) = norm(A, Inf) * norm(Inverse, Inf);
  end

  semilogy(range, fehler_lr, range, fehler_cholesky, range, fehler_inverse, range, kondition);
  legend('LR-Zerlegung','Cholesky', 'Inverse', 'Kondition');
  xlabel('k');
end

function [L,R] = lr_zerlegung(A)
  n = size(A)(1);
  L = diag(ones(1, n));
  for i = 1:n
    g = A(i+1:n,i) / A(i,i);
    A(i+1:n,i:n) -= g * A(i,i:n);
    L(i+1:n,i) = g;
  end
  R = A;
end

% Lösung von LGS'en:
% A = L*R => A^-1 = R^-1*L^-1
% A*x=b => R*x = L^-1*b
% Sei y:=L^-1*b => R*x=y und L*y=b
% => Löse zunächst Ly=b, dann Rx=y
function x = lgs_loesen(L, R, b)
  n = length(b); % =size(L)(1)=size(L)(2)=size(R)(1)=size(R)(2)
  y(1) = b(1);
  for i = 2:n
    y(i) = b(i) - sum(L(i,1:i-1) .* y(1:i-1));
  end
  x(n) = y(n) / R(n,n);
  for i = n-1:-1:1
    x(i) = (y(i) - sum(R(i,i+1:n) .* x(i+1:n))) / R(i,i);
  end
end
