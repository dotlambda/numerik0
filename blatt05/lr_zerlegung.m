function [L,R] = lr_zerlegung(A)
  n = size(A)(1);
  L = diag(ones(1, n));
  for i = 1:n
    g = A(i+1:n,i) / A(i,i);
    A(i+1:n,i:n) -= g * A(i,i:n);
    L(i+1:n,i) = g;
  end
  R = A;
end
