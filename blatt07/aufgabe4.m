function erg = aufgabe4()
  x = 2; y = 4.53; r = 3;
  P = [0, .05, .05, .2, .2];
  N = [5, 5, 20, 20, 100];

  for i = 1:length(P)
    p = P(i); n = N(i);
    A = mydata(x, y, r, p, n);
    x_0 = [10, -10, 3]';
    %x_0 = x_0 - [-100, .0005, 1]';
    F = @(x) Kreis(x', A)';
    J = @(x) Jacobi(x', A);
    [x_min,err] = gauss_newton(F, J, x_0, 1);
    
    figure;
    semilogy(err)
    figure;
    clf;
    hold on;
    circle(x, y, r, 'r');
    circle(x_min(1), x_min(2), x_min(3), 'b');
    plot(A(:,1), A(:,2), 'x');
    hold off;
  end
end

function erg = circle(x, y, r, string)
  t = linspace(0,2*pi,100)';
  circsx = r.*cos(t) + x;
  circsy = r.*sin(t) + y;
  plot(circsx,circsy,string);
end

function [x_min,err] = gauss_newton(F, J, x_0, lambda)
  x = x_0;
  i=1;
  do
    delta = (J(x)' * J(x)) ^ -1 * (- J(x)' * F(x));  % Ax=b    x= A\b
    err(i)=norm(- J(x)' * F(x));
    x = x + lambda * delta;
    i = i+1;
  until(norm(delta) <  10^-8)
  x_min = x;
end

function fehler = Kreis(x, A)
  M = x(1:2);
  r = x(3);
  for i = 1:size(A)(1)
    fehler(i) = (norm(A(i,:) - M, 2) - r);
  end
end

function J = Jacobi(x, A)
  M = x(1:2);
  r = x(3);
  for i = 1:size(A)(1)
    for j = 1:2
      J(i,j) = .5 * norm(A(i,:) - M, 2) * (-2 * A(i,j) + 2 * M(j));
    end
    J(i,3) = -1;
  end
end