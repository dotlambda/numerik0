function erg = aufgabe4()
  for n = 1:30
    y1(n) = abs((formel(-5.5, n) - exp(-5.5)) / exp(-5.5));
    y2(n) = abs((1 / formel(5.5, n) - exp(-5.5)) / exp(-5.5));
    y3(n) = abs((formel(0.5, n) ^ -11 - exp(-5.5)) / exp(-5.5));
  end

  n = 50000;
  tic;
  summe = abs((formel(0.5, n) ^ -11 - exp(-5.5)) / exp(-5.5))
  toc
  tic;
  horner = abs((horner(0.5, n) ^ -11 - exp(-5.5)) / exp(-5.5))
  toc

  semilogy(1:30, y1, 1:30, y2, 1:30, y3);
  legend('1)','2)', '3)');
  xlabel('n');
end

function erg = formel(x, n)
  erg = 0;
  for k = 0:n
    erg += x ^ k / factorial(k);
  end
end

function erg = horner(x, n);
  erg = 1 / factorial(n);
  while (n>0)
    n=n-1;
    erg = erg * x + 1 / factorial(n);
  end
end
