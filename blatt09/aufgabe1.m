function [poly, spline] = aufgabe1(n)
    poly = 2^(n+1)*exp(4)/factorial(n+1);
    for j = 0:n
        poly *= (1-(2*j))/n;
    end
    spline = 2^3*exp(4)/factorial(3);
    for j = 0:2
        spline *= (1-(2*j))/n;
    end
end
