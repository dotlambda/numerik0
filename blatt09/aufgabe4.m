function aufgabe4(i, n)
    f = @(x) exp(-3*x.^2);
    g = @(x) abs(x) .^ .5;
    h = @(x) 1 * (x == 0);
    funcs = {f, g, h};
    
    func = funcs{i};
    x = [-1:2/n:1];
    y = func(x);
    
    range = [-1:.001:1]';
    plot(range, MySpline(x, y)(range), 'r-', range, func(range), 'g-', x, y, 'bx', range, MyLagrange(x, y, range), 'c');
end

function interpolated = MySpline(x, y)
    n = length(x) - 1;
    a0(1:n) = y(2:n+1);
    h(1:n) = x(2:n+1) - x(1:n);
    A = diag(2*(h(1:n-1)+h(2:n))) + diag(h(2:n-1),-1) + diag(h(2:n-1),+1);
    b = (3 * ((y(3:n+1)-y(2:n))./h(2:n) - (y(2:n)-y(1:n-1))./h(1:n-1)))';
    a2 = (A \ b)';
    a2(n) = 0;
    a1(1) = (y(2)-y(1))/h(1) + h(1)/3 * 2*a2(1);
    a1(2:n) = (y(3:n+1)-y(2:n))./h(2:n) + h(2:n)/3 .* (2*a2(2:n) + a2(1:n-1));
    a3(1) = a2(1) / (3 * h(1));
    a3(2:n) = (a2(2:n) - a2(1:n-1)) ./ (3 * h(2:n));
    func = @(in) spline(a0, a1, a2, a3, x, in);
    interpolated = @(in_vector) arrayfun(func, in_vector);
end

function out = spline(a0, a1, a2, a3, x, in)
    for i = 1:length(a0)
        if in <= x(i+1)
            out = a0(i) + a1(i)*(in-x(i+1)) + a2(i)*(in-x(i+1))^2 + a3(i)*(in-x(i+1))^3;
            break;
	end
    end
end
