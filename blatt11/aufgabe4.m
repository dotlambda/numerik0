function out = aufgabe4()
    for i = [1:6]
        n = 2^i;
        range(i) = n;
        A = MyMatrix(n);
        A = A' * A;
        B = diag(4 * ones(n,1)) + diag(ones(n-1,1), -1) + diag(ones(n-1,1), 1);
        matrices = {A, B};
        for j = [1 2]
            M = matrices{j};
            max_ew = Mises(M);
            %max_ew = max(eig(M));
            if j == 1
                max_ew = sqrt(max_ew);
            end
            min_ew = MisesInv(M, 0);
            %min_ew = min(eig(M));
            if j == 1
                min_ew = sqrt(min_ew);
	    end
            kond(i,j) = abs(max_ew / min_ew);
        end
        for j = [1 2]
            if j == 1
                kond(i,j+2) = sqrt(cond(matrices{j},2));
            else
                kond(i,j+2) = cond(matrices{j},2);
            end
        end
    end
    semilogy(range, kond);
    legend('spektr-cond(A)', 'spektr-cond(B)', 'cond(A)', 'cond(B)');
    xlabel('n');
    ylabel('cond(M)')
end

function lam = Mises(A)
    x = zeros(size(A)(1),1);
    x(1) = 1;
    x_ = A * x;
    [m,k] = max(x);
    lam = x_(k) / x(k);
    x = x_ / norm(x_);
    do
        lam_ = lam;
        x_ = A * x;
        lam = x_(k) / x(k);
        x = x_ / norm(x_);
    until(abs(lam - lam_) < 1e-10)
end

function lam = MisesInv(A, sig)
    n = size(A)(1);
    B = A - sig * eye(n);
    x = zeros(n,1);
    x(1) = 1;
    x_ = B \ x;
    [m,k] = max(x_);
    my = x_(k) / x(k);
    x = x_ / norm(x_);
    lam = 1/my + sig;
    do
        lam_ = lam;
        x_ = B \ x;
        my = x_(k) / x(k);
        x = x_ / norm(x_);
        lam = 1/my + sig;
    until(abs(lam - lam_) < 1e-10)
end
