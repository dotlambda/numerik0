function erg = MyLagrange(x,y,s)

%               Eingabegroessen:        x       Stuetzstellen
%                                       y       Knotenwerte
%                                       s       Auswertungspunkte
%
%               Ausgabegroesse:         erg     Funktionswerte des Lagrange'schen 
%                                               Interpolationspolynoms in den Aus-
%                                               wertungspunkten s
%
% Beispiel:             Interpolation der Kosinusfunktion auf dem Intervall [-1,1]
%
%               >> x  = [-1,0,1]';
%               >> y  = [0.5403, 1, 0.5403]';
%               >> s  = linspace(-1,1,101)';
%               >> res= MyLagrange(x,y,s);
%               >> plot(x,y,'bo', s, cos(s), 'g-', s, res, 'r-');
%

        n=length(x);

        A(:,1)=y;
        a(1,1)=y(1);

        for i=2:n
            for k=i:n
                  A(k,i) = (A(k,i-1)-A(k-1,i-1))/(x(k)-x(k-i+1));
            end
            a(i,1)=A(i,i);
        end


        for i =1:length(s)
                n = length(a);
                b(n) = a(n);

                while (n>1)
                        n=n-1;
                        b(n) = b(n+1) * (s(i) - x(n)) + a(n);
                end
                erg(i) = b(n);
        end
end
