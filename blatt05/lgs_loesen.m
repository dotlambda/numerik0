% Lösung von LGS'en:
% A = L*R => A^-1 = R^-1*L^-1
% A*x=b => R*x = L^-1*b
% Sei y:=L^-1*b => R*x=y und L*y=b
% => Löse zunächst Ly=b, dann Rx=y
function x = lgs_loesen(L, R, b)
  n = length(b); % =size(L)(1)=size(L)(2)=size(R)(1)=size(R)(2)
  y(1) = b(1);
  for i = 2:n
    y(i) = b(i) - sum(L(i,1:i-1) .* y(1:i-1));
  end
  x(n) = y(n) / R(n,n);
  for i = n-1:-1:1
    x(i) = (y(i) - sum(R(i,i+1:n) .* x(i+1:n))) / R(i,i);
  end
end
